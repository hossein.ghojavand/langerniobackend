FROM php:7.4-fpm

COPY ./composer.lock  /var/www/
COPY ./composer.json /var/www/

WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libzip-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    iputils-ping \
    libonig-dev

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd


# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --1

# Copy application folder
COPY ./ /var/www

RUN composer install

# Add user for laravel
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy env file
COPY ./.env /var/www/

# Create privilages
RUN chmod -R 777 /var/www
RUN chown -R www:www /var/www

RUN php artisan passport:install --force

# change current user to www
USER www

EXPOSE 9000
CMD ["php-fpm"]
