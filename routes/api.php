<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FamilyTreeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);


Route::get('family_tree/index' , [FamilyTreeController::class,'index']);
Route::post('family_tree/show' , [FamilyTreeController::class,'show']);

Route::middleware('auth:api')->group(function () {
    Route::resource('posts', PostController::class);
    Route::get('profile' , [UserController::class,'profile']);
    Route::post('profile/edit' , [UserController::class,'editProfile']);
    Route::get('profile/my_family_trees' , [UserController::class,'myFamilyTrees']);
    Route::post('family_tree/create' , [FamilyTreeController::class,'create']);
    Route::post('family_tree/add_new_node' , [FamilyTreeController::class,'add_new_node']);
});