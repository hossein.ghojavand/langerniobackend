<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyTree extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'description'
    ];

    public function admin(){
        return $this->belongsTo(User::class);
    }

    public function nodes(){
        return $this->hasMany(FamilyNode::class);
    }
}
