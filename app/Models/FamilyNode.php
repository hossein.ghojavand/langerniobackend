<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyNode extends Model
{
    use HasFactory;
    protected $fillable = ['parent_node_id','first_name' , 'last_name']; 

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function family_tree(){
        return $this->belongsTo(FamilyTree::class);
    }
}