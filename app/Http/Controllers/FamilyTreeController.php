<?php

namespace App\Http\Controllers;
use App\Models\FamilyTree;
use App\Models\FamilyNode;
use App\Models\User;

use Illuminate\Http\Request;

class FamilyTreeController extends Controller
{
    public function index()
    {
        
        $trees = FamilyTree::select('id' , 'title' , 'description','admin_id')
        ->with('admin:id,first_name,last_name,email,phone_number')
        ->simplePaginate(10);
 
        if(count($trees) == 0)
            return response()->json([
                'status' => false,
                'error' => "هیچ شجره‌نامه‌ای یافت نشد"
            ]);
        return response()->json([
            'success' => true,
            'data' => $trees
        ]);
    }

    public function generate_children($root_node , $family_tree_id)
    {
        $children = FamilyNode::where("family_tree_id",$family_tree_id)->where("parent_node_id" ,$root_node->id )->select('id' , 'parent_node_id' , 'first_name' , 'last_name' , 'user_id' )->get();
        if (count($children)!=0)
        {
            foreach($children as $child)
               $this->generate_children($child , $family_tree_id);
            $root_node['children'] = $children;
        }
        else
            $root_node['children'] = array();

        return $root_node;
    }

    public function show(Request $request)
    {
        if($request->family_tree_id)
        {
            $family_tree = FamilyTree::where('id' , $request->family_tree_id)->select('id' , 'title' , 'description','admin_id')->with('admin:id,first_name,last_name,email,phone_number')->first();
            
            $response['id'] = $family_tree->id;
            $response['title'] = $family_tree->title;
            $response['description'] = $family_tree->description;

            $response['admin'] = $family_tree->admin;


            $root_node = FamilyNode::where("family_tree_id",$request->family_tree_id)->where("parent_node_id" , null)
            ->select('id' , 'parent_node_id' , 'first_name' , 'last_name' , 'user_id' )->first();

            if ($root_node == null)
                $response['nodes'] = [];
            else
            {
                $root_node =  $this->generate_children($root_node , $request->family_tree_id);
                $response['nodes'] = $root_node;
            }

            return response()->json([
                'status' => true,
                'data' => $response
            ]);
        }
        else
            return response()->json([
                'status' => false,
                'error' => "هیچ شجره‌نامه‌ای یافت نشد"
            ]);
    }


    public function create(Request $request)
    {
        if($request->title)
        {
            $user = auth()->user();
            $new_family_tree = new FamilyTree();
            $new_family_tree->admin_id = $user->id;
            $new_family_tree->title = $request->title;
            
            if($request->description)
                $new_family_tree->description = $request->description;
            else
                $new_family_tree->description = "" ;
        
            $new_family_tree->admin->id = $user->id;
            $new_family_tree->save();

            $family_tree = FamilyTree::where('id' , $new_family_tree->id )->select('id' , 'title' , 'description','admin_id')->with('admin:id,first_name,last_name,email,phone_number')->first();

            return response()->json([
                'success' => true,
                'data' => $family_tree
            ]);
            
        }
        else
        {
            return response()->json([
                'status' => false,
                'error' => "نام شجره نامه وارد نشده است"
            ]);
        }
    }

    public function add_new_node(Request $request)
    {

        $user = auth()->user();
        if($request->family_tree_id)
        {
            $user_trees = FamilyTree::where("admin_id" , auth()->user()->id)->where("id" , $request->family_tree_id)->select('id')->get();

            if(count($user_trees) == 0 )
            {
                return response()->json([
                    'status' => false,
                    'error' => "شما دسترسی به این شجره نامه ندارید"
                ]);
            }
            else
            {
                $nodes = FamilyNode::where("family_tree_id",$request->family_tree_id)->select('id')->get();
                if($request->parent_id or (!$request->parent_id and count($nodes)==0) )
                {
                    if($request->user_id or ($request->first_name and $request->last_name))
                    {
                        $new_node = new FamilyNode();
                        if (count($nodes)==0)
                            $new_node->parent_node_id = null;
                        else
                            $new_node->parent_node_id = $request->parent_id;
                        
                        $new_node->family_tree_id = $request->family_tree_id;

                        if($request->user_id)
                        {
                            $target_user = User::where("id" , $request->user_id)->select('first_name' , 'last_name')->first();
                            $new_node->user_id = $request->user_id;
                            $new_node->first_name = $target_user->first_name;
                            $new_node->last_name = $target_user->last_name;
                        }
                        else
                        {
                            $new_node->user_id = null;
                            $new_node->first_name = $request->first_name;
                            $new_node->last_name = $request->last_name;
                        }
                        $new_node->save();

                        return response()->json([
                            'status' => true,
                            'data' => "نود با موفقیت اضافه شد"
                        ]);
                    }
                    else
                        return response()->json([
                            'status' => false,
                            'error' => "اطلاعات نود دریافت نشد. کاربر را وارد کنید یا نام و نام خانوادگی را مشخص کنید"
                        ]);


                }
                else
                {
                    return response()->json([
                        'status' => false,
                        'error' => "پدر تعریف نشده است"
                    ]);
                }
            }
                
        }
        else
            return response()->json([
                'status' => false,
                'error' => "هیچ شجره‌نامه‌ای یافت نشد"
            ]);
  
    }
}
