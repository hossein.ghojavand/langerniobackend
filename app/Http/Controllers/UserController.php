<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\FamilyTree;
use App\Models\FamilyNode;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Morilog\Jalali\Jalalian;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    
    public function profile()
    {
        $user = User::where('id' , auth()->user()->id)
            ->select('name','first_name', 'last_name', 'email' , 'father_name' , 'mother_name', 'birthdate' ,'marriage_type','phone_number')
            ->first();

        // change to persian date
        if($user->birthdate != null)
        {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $user->birthdate, 'UTC');
            $date->setTimezone('Asia/Tehran');
            $date = (Jalalian::fromDateTime($date));
            $user->birthdate =$date;
        }

        /*
        // avatar
        if ($user->avatar != null){
            $user->avatar = asset('storage/avatars/' . $user->avatar);
        }
        */

       
        return response()->json([
            'status' => true ,
            'data' => $user
        ]);
    }

    public function myFamilyTrees()
    {
        $trees = FamilyTree::where("admin_id" , auth()->user()->id)
        ->select('id' , 'title' , 'description','admin_id')
        ->with('admin:id,first_name,last_name,email,phone_number')->get();

        $response["admin"] = $trees;

        $other_trees = FamilyNode::where("user_id",auth()->user()->id)->select('family_tree_id' )->get();

        $temp = array();
        foreach($other_trees as $other_tree)
        {
            $tree =  FamilyTree::where("id" , $other_tree->family_tree_id)
            ->select('id' , 'title' , 'description','admin_id')
            ->with('admin:id,first_name,last_name,email,phone_number')->first();
            array_push($temp , $tree);
        }

 
        $response["member"] = $temp;

        if(count($trees) == 0 && count($temp) ==0)
            return response()->json([
                'status' => false,
                'error' => "هیچ شجره‌نامه‌ای یافت نشد"
            ]);
        return response()->json([
            'success' => true,
            'data' => $response
        ]);
    }

    public function editProfile(Request $request)
    {

        if(!$request->name && !$request->first_name && !$request->last_name && !$request->father_name && !$request->mother_name &&
           !$request->birthdate && !$request->marriage_type && !$request->phone_number)
        {
            return response()->json([
                'status' => false,
                'error' => 'fileds are empty'
            ]);
        }

        $user = auth()->user();
        if ($request->name)
            $user->name = $request->name;

		if($request->first_name)
			$user->first_name = $request->first_name;
			
        if($request->last_name)
			$user->last_name = $request->last_name;
            
        if($request->father_name)
			$user->father_name = $request->father_name;

        if($request->mother_name)
			$user->mother_name = $request->mother_name;

		if($request->birthdate)
			$user->birthdate = $request->birthdate;

		if($request->marriage_type)
			$user->marriage_type = $request->marriage_type;

		if($request->phone_number)
			$user->phone_number = $request->phone_number;

        /*if ($request->password){
            if ($user->email == null)
                return response()->json([
                    'status' => false,
                    'error' => Config::get('constants.errors.ERR_NO_NEED_PASS_CHANGE')
                ]);
            $user->password = bcrypt($request->password);
        }*/

    

        /*if ($request->hasFile('avatar')) {
            $old_avatar = public_path() . '/storage/avatars/' . $user->avatar;
            $avatar = $request->file('avatar');
            $avatar_extension = $request->file('avatar')->getClientOriginalExtension();
            $avatar_name = 'userAavatar' . time() . '.' . $avatar_extension;
            $avatar_dir = 'storage/avatars/';
            Image::make($avatar)->resize(220, 220)->save($avatar_dir . $avatar_name);

            if (file_exists($old_avatar) && $user->avatar != 'noImage.png' && $user->avatar != null) {
                unlink($old_avatar);
            }
            $user->avatar = $avatar_name;
        }*/
        $user->save();

        $user = User::where('id' , auth()->user()->id)
            ->select('name','first_name', 'last_name', 'email' , 'father_name' , 'mother_name', 'birthdate' ,'marriage_type','phone_number')
            ->first();


        // change to persian date
        if($user->birthdate != null)
        {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $user->birthdate, 'UTC');
            $date->setTimezone('Asia/Tehran');
            $date = (Jalalian::fromDateTime($date));
            $user->birthdate =$date;
        }

        /*if ($user->avatar != null)
            $user->avatar = asset('storage/avatars/' . $user->avatar);*/

        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }

}
